run:
	uvicorn notes.main:app

.PHONY: run

test:
	poetry run pytest ./ -v
	rm -r .pytest_cache

.PHONY: test

lint:
	poetry run mypy ./
	rm -r .mypy_cache
	poetry run flake8

.PHONY: lint