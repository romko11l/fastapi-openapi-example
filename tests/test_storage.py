from notes.main import Storage, Note

note = Note(description="Roma hochet kushat")
another_note = Note(description="Roma hochet kushat")


class TestStorage:
    def test_get_all_notes(self):
        s = Storage()
        assert s.get_all_notes() == dict()
        a = s.add_note(note)
        assert s.get_all_notes() == dict({0: note})
        b = s.add_note(another_note)
        assert s.get_all_notes() == dict({a: note, b: another_note})

    def test_get_note_by_id(self):
        s = Storage()
        assert s.get_note_by_id(0) is None
        a = s.add_note(note)
        assert s.get_note_by_id(a) == note
        b = s.add_note(another_note)
        assert s.get_note_by_id(b) == another_note

    def test_add_note(self):
        s = Storage()
        assert s.get_all_notes() == dict()
        a = s.add_note(note)
        b = s.add_note(another_note)
        assert s.get_all_notes() == dict({a: note, b: another_note})

    def test_update_note_by_id(self):
        s = Storage()
        assert s.update_note_by_id(0, note) is False
        a = s.add_note(note)
        assert s.update_note_by_id(a, another_note) is True
        assert s.get_note_by_id(a) == another_note

    def test_delete_note_by_id(self):
        s = Storage()
        assert s.delete_note_by_id(0) is False
        a = s.add_note(note)
        assert s.delete_note_by_id(a) is True
        assert s.delete_note_by_id(a) is False
        assert s.get_note_by_id(a) is None
