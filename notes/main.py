from typing import Union

from fastapi import FastAPI, HTTPException, status
from pydantic import BaseModel

import json


class Note(BaseModel):
    description: str


class Storage:
    def __init__(self):
        self._notes = dict()
        self._counter = -1

    def get_all_notes(self) -> dict:
        return self._notes

    def get_note_by_id(self, note_id: int) -> Union[Note, None]:
        if note_id not in self._notes:
            return None
        return self._notes[note_id]

    def add_note(self, note: Note) -> int:
        self._counter += 1
        self._notes[self._counter] = note
        return self._counter

    def update_note_by_id(self, note_id: int, note: Note) -> bool:
        if note_id in self._notes:
            self._notes[note_id] = note
            return True
        return False

    def delete_note_by_id(self, note_id: int) -> bool:
        if note_id in self._notes:
            del self._notes[note_id]
            return True
        return False


storage = Storage()

app = FastAPI()


@app.get("/note", status_code=status.HTTP_200_OK)
def get_notes():
    return {"notes": storage.get_all_notes()}


@app.get("/note/{note_id}", status_code=status.HTTP_200_OK)
def get_note(note_id: int):
    note = storage.get_note_by_id(note_id)
    if note is None:
        raise HTTPException(status_code=404, detail=f"Note with id {note_id} not found")
    return {"note_id": note_id, "note": note.description}


@app.post("/note", status_code=status.HTTP_201_CREATED)
def create_note(note: Note):
    note_id = storage.add_note(note)
    return {"note_id": note_id}


@app.put("/note/{note_id}", status_code=status.HTTP_200_OK)
def update_note(note_id: int, note: Note):
    if not storage.update_note_by_id(note_id, note):
        raise HTTPException(status_code=404, detail=f"Note with id {note_id} not found")


@app.delete("/note/{note_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_note(note_id: int):
    if not storage.delete_note_by_id(note_id):
        raise HTTPException(status_code=404, detail=f"Note with id {note_id} not found")


@app.on_event("startup")
def generate_spec():
    with open("../spec.json", "w", encoding="utf-8") as f:
        json.dump(app.openapi(), f, ensure_ascii=False, indent=4)
